# Degoogle Workshop Slides

## How to use (locally)

1. Clone this Repo
2. Enter the repo directory
3. Issue the following command

```bash
npm start
```

4. Go to http://localhost:8000

## How to use (Remote)

Just go to [This page](https://degoogle.nolim1t.co/)
